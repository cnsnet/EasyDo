# 关闭windows自动更新

1. Windows自动更新并不能显著增加你系统的安全性
2. Windows自动更新经常会造成原本已经稳定运行的程序不能运行, 具体表现为前几天还能运行的程序今天就打不开了, 前几天连server还好好的今天就连不上了
3. Windows自动更新在后台偷摸安装的时候使电脑运行卡顿, 占用网络资源.

## Windows 7
1. 打开控制面板<br />
![](Images/7-1.jpg)<br />
2. 点击右上角的查看方式, 改为大图标, 然后找到Windows Update<br />
![](Images/7-2.jpg)<br />
3. 在Windows Update窗口中, 点击更改设置<br />
![](Images/7-3.jpg)<br />
4. 在打开的设置窗口中, 选择"从不检查更新", 并且将下面的"允许所有用户在此计算机上安装更新"的对勾去掉, 然后点击确定<br />
![](Images/7-4.jpg)<br />

## Windows 8
[点击查看Windows 8关闭自动更新](http://www.xitongzhijia.net/xtjc/20160607/74142.html)

## Windows 10
1. 鼠标右键此电脑, 点击管理<br />
![](Images/10-1.png)<br />
2. 在打开的窗口中, 找到服务和应用, 点击服务<br />
3. 在右侧的服务列表中, 找到Windows Update, 双击打开<br />
![](Images/10-2.png)<br />
4. 按照图示进行操作, 常规选项卡中的启动类型设置为"禁用", 恢复选项卡中的第一次,第二次以及后续失败都改为"无操作", 然后点击"确定"<br />
![](Images/10-3.jpg)![](Images/10-4.jpg)