## 浏览器获取京东cookie教程

 **以下浏览器都行**

 - Chrome浏览器
 - 新版Edge浏览器
 - 国产360，QQ浏览器切换到极速模式

### 操作步骤

1. 电脑浏览器打开京东网址 [https://m.jd.com/](https://m.jd.com/)
2. 按键盘F12键打开开发者工具，然后点下图中的图标切换到移动设备模式<br />
![](https://gitee.com/cnsnet/EasyDo/raw/master/Images/GetJDCookie-1.jpg)
3. 如果是未登录状态, 登陆你的京东账号;
4. 如果已登录请直接看第五步
   - 使用手机短信验证码登录(此方式cookie有效时长大概30天，其他登录方式比较短)
5. 登录后，按照下面的图片找到 `pt_key` 和 `pt_pin`, 可以点击Name进行排序, 方便查找<br />
![](https://gitee.com/cnsnet/EasyDo/raw/master/Images/GetJDCookie-2.jpg)
6. 如果需获取第二个京东账号的cookie,不要在刚才的浏览器上面退出登录账号, 否则刚才获取的cookie会失效, 需另外换一个浏览器(Chrome浏览器 `ctr+shift+n` 打开无痕模式也行),然后继续按上面步骤操作即可